﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DespesasJOFEBAR.Models
{
    public class Gastos
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Despesa { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Valor { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime Data { get; set; } = DateTime.Now;

        [Required]
        public string Categoria { get; set; }

        public Gastos()
        {
            Id = Guid.NewGuid();
        }
    }
}
